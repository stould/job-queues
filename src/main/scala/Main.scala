import cats.syntax.either._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import scala.collection.mutable.ListBuffer

sealed trait GeneralData;
case class new_agent(id: String, name: String, primary_skillset: List[String], secondary_skillset: List[String]) extends GeneralData;
case class new_job(id: String, skill: String, urgent: Boolean) extends GeneralData;
case class job_request(agent_id: String) extends GeneralData;

object Main {

    def main(args: Array[String]): Unit = {

        //reading from file
        val rawJson = new InputReader().readFile("test3.json");

        //decoding JSON string
        val doc = decode[List[GeneralData]](rawJson).toList;

        //instance for the controller
        val controller = new Controller();

        //getting requests from json file
        for(request <- doc(0)){
            if(request.isInstanceOf[new_agent]){
                val tempAgent: new_agent = request.asInstanceOf[new_agent];
                val agent = new Agent(tempAgent.id, tempAgent.name, tempAgent.primary_skillset, tempAgent.secondary_skillset);
                controller.addNewAgent(agent);
            }else if(request.isInstanceOf[new_job]){
                val tempJob: new_job = request.asInstanceOf[new_job];
                val job = new Job(tempJob.id, tempJob.skill, tempJob.urgent);
                controller.enqueueJob(job);
            }else{
                val tempJobRequest: job_request = request.asInstanceOf[job_request];
                controller.jobRequest(tempJobRequest.agent_id);
            }
        }

        case class job_assigned(job_id: String, agent_id: String);
        var outputList = ListBuffer[Map[String, job_assigned]]();

        //filling output
        for(output <- controller.getOutputLog()){
            val map = Map[String, job_assigned]("job_assigned" -> new job_assigned(output._2.id,output._1.id))
            outputList += map;
        }

        //printing json output
        println(outputList.asJson.spaces2);
    }

}
