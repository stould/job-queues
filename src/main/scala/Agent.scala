case class Agent(id: String, name: String, primary_skillset: List[String], secondary_skillset: List[String]) {
    override def toString: String = name + " " + id + " " + primary_skillset + " " + secondary_skillset;
}