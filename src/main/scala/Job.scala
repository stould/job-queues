case class Job(id: String, skill: String, priority: Boolean) {
    override def toString: String = id + " " + skill + " " + priority;

    def copy(): Job = {
        return new Job(id, skill, priority);
    }
}

