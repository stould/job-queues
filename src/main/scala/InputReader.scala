class InputReader {
    def readFile(fileName: String): String = {
        val source = scala.io.Source.fromURL(getClass.getResource(fileName));
        val ans = source.mkString;
        source.close();
        return ans;
    }
}
