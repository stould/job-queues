import scala.annotation.tailrec
import scala.collection.mutable.{ListBuffer}

class Controller {

    //the queue for low priority jobs
    private var low_priority = new ListBuffer[Job]();

    //the queue for high priority jobs
    private var high_priority = new ListBuffer[Job]();

    //mapping the skills
    private var skill_map: Map[String, List[ListBuffer[String]]] = Map();

    //just a helper in order to get an Agent() via agent_id
    private var agent_map: Map[String, Agent] = Map();

    //map to check if an agent is available
    private var isAvailable: Map[String, Boolean] = Map();

    //just helper to generate an output log
    private var outputLog: ListBuffer[(Agent, Job)] = ListBuffer();

    def getOutputLog(): ListBuffer[(Agent,Job)] ={
        return outputLog;
    }

    //method to assign jobs to agents while it is possible
    @tailrec
    private def doDequeue(): Unit = {
        val assignment: (Agent, Job) = dequeueJob();
        if (assignment != null) {
            outputLog += assignment;
            doDequeue();
        }
    }

    //Organize agents, all the busy agents goes to the tail of the lists.
    @tailrec
    private def organizeAgents(agents: ListBuffer[String], alive: Int): Unit = {
        if (alive > 1) {
            if (isAvailable(agents(0)) == false) {
                val agent_id: String = agents(0);
                agents.remove(0);
                agents += agent_id;
                organizeAgents(agents, alive - 1);
            }
        }
    }

    //generate an output of the assignment
    def outputAssignment(assignment: (Agent, Job)): String = {
        val output: String = "Job \"" + assignment._2.id + "\" assigned to \"" + assignment._1.id + "\"";
        if (assignment._2.priority) {
            output.concat(" with priority.");
        }
        return output;
    }

    //dequeuing process
    private def dequeueJob(): (Agent, Job) = {
        for ((job, i) <- high_priority.zipWithIndex) {
            if (skill_map.exists(skillName => skillName._1 == job.skill)) {
                for (j <- 0 to 1) {
                    if(skill_map(job.skill)(j).length > 0) {
                        organizeAgents(skill_map(job.skill)(j), skill_map(job.skill)(j).length);
                        val agent_id = skill_map(job.skill)(j)(0);
                        if (isAvailable.exists(agent => agent._1 == agent_id) && isAvailable(agent_id) == true) {
                            isAvailable = isAvailable.updated(agent_id, false);
                            high_priority.remove(i);
                            return (agent_map(agent_id), job);
                        }
                    }
                }
            }
        }
        for ((job, i) <- low_priority.zipWithIndex) {
            if (skill_map.exists(skillName => skillName._1 == job.skill)) {
                for (j <- 0 to 1) {
                    if(skill_map(job.skill)(j).length > 0) {
                        organizeAgents(skill_map(job.skill)(j), skill_map(job.skill)(j).length);
                        val agent_id = skill_map(job.skill)(j)(0);
                        if (isAvailable.exists(agent => agent._1 == agent_id) && isAvailable(agent_id) == true) {
                            isAvailable = isAvailable.updated(agent_id, false);
                            low_priority.remove(i);
                            return (agent_map(agent_id), job);
                        }
                    }
                }
            }
        }
        return null;
    }

    //enqueuing process
    def enqueueJob(task: Job): Unit = {
        if (task.priority == false) {
            low_priority += task;
        } else {
            high_priority += task;
        }
        doDequeue();
    }

    //set an agent available
    def jobRequest(agent: Agent): Unit = {
        isAvailable = isAvailable.updated(agent.id, true);
        doDequeue();
    }
    //set an agent available
    def jobRequest(agent_id: String): Unit = {
        isAvailable = isAvailable.updated(agent_id, true);
        doDequeue();
    }

    //add an new agent
    def addNewAgent(agent: Agent): Unit = {
        //block an agent to join more than once
        if (!isAvailable.exists(x => x._1 == agent.id)) {
            agent_map = agent_map.updated(agent.id, agent);
            isAvailable = isAvailable.updated(agent.id, false);

            for (i <- agent.primary_skillset) {
                if (!skill_map.exists(skillName => skillName._1 == i)) {
                    skill_map = skill_map.updated(i, ListBuffer[String]() :: ListBuffer[String]() :: Nil);
                }
                skill_map(i)(0) += agent.id;
            }
            for (i <- agent.secondary_skillset) {
                if (!skill_map.exists(skillName => skillName._1 == i)) {
                    skill_map = skill_map.updated(i, ListBuffer[String]() :: ListBuffer[String]() :: Nil);
                }
                skill_map(i)(1) += agent.id;
            }

        }
        doDequeue();
    }
}