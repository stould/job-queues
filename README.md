## Steps to run the program
1) Download **IntelliJ IDEA Community** from  {https://www.jetbrains.com/idea/download/}

2) Clone the project

3) Open the cloned project on **IntelliJ** and wait for the SBT (similar to Maven) to build and syncronize the project's libraries.


## Some relevant observations:
1) For the input, I am reading from a file (sorry for that but I've faced problems reading JSON from stdin), you can place the file at the folder **resources**.

2) In order to let the program pick the correct input, please change the file name at line 18 of **Main.scala**.

3) I added some good tests cases, they are located at **resouces** folder.

4) I added a brief explanation about how the algorithm works, you can find it at **resouces -> Solution.pdf **

5) I am using the Circe library to parse JSON.

6) **A VERY relevant observation** is, in the input file, for each **new_job**, please change the JSON field **type** to **skill**.